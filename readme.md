![logo.png](https://bitbucket.org/repo/neLoRa/images/366109021-logo.png)

# XLadder

XLadder is a minimalistic matchmaking/ladder system for L4D.

> Built on Laravel 5.4

### Installation
```sh  
$ composer install
$ cp .env.example .env  
$ php artisan key:generate  
$ php artisan migrate
$ php artisan passport:install  
$ npm install
$ npm install -g laravel-echo-server  
$ laravel-echo-server init  (or copy .example file)
$ npm run dev
$ php artisan queue:work redis --sleep=3 --tries=3
$ laravel-echo-server start
```  

##### For development
```sh
$ php artisan ide-helper:generate
$ php artisan ide-helper:meta
```

##### Some troubleshooting

if you are develop on homestead you could meet this troubles
+ `--no-bin-links` key may be need for successful `npm install`  
+ rerun `npm install` (again with `--no-bin-links` flag) if ` Maximum call stack size exceeded` problem  
+ `npm rebuild node-sass` if have problem with building sass  

##### For test purposes
+ uncomment lines in DatabaseSeeder and run `artisan db:seed` after migration for dummy data (no relations)
