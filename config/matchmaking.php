<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Points table
    |--------------------------------------------------------------------------
    |
    | Points for winning or loosing games.
    | Reporting user also entails points loose.
    |
    */
    'points' => [
        'win' => 20,
        'loose' => 18,
        'report' => 100
    ],

    /*
    |--------------------------------------------------------------------------
    | Other variables
    |--------------------------------------------------------------------------
    |
    | Players to start match by default
    */
    'players' => [
        'start_default' => 8
    ]

];
