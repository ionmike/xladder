<?php

namespace App\Policies;

use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class MatchPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the message.
     *
     * @param  \App\Models\User  $user
     * @return bool
     */
    public function update(User $user) : bool
    {
        return !$user->roles()->findMany([1, 2, 3])->isEmpty();
    }
}
