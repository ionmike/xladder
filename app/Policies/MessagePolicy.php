<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Message;

use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the message.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Message  $message
     * @return bool
     */
    public function update(User $user, Message $message) : bool
    {
        if (!$user->roles()->findMany([1, 2])->isEmpty()) return true;

        return $message->user->id === $user->id && $message->created_at > Carbon::now()->subMinutes(10);
    }

    /**
     * Determine whether the user can destroy the message.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Message  $message
     * @return bool
     */
    public function destroy(User $user, Message $message) : bool
    {
        if (!$user->roles()->findMany([1, 2])->isEmpty()) return true;

        return $message->user->id === $user->id && $message->created_at > Carbon::now()->subMinutes(10);
    }
}
