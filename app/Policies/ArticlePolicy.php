<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Article;

use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can store articles.
     *
     * @param  \App\Models\User  $user
     * @return bool
     */
    public function store(User $user) : bool
    {
        return $user->roles()->find(1) !== null;
    }

    /**
     * Determine whether the user can update the article.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return bool
     */
    public function update(User $user, Article $article) : bool
    {
        return $user->roles()->find(1) !== null || $article->user->id === $user->id;
    }

    /**
     * Determine whether the user can destroy the article.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return bool
     */
    public function destroy(User $user, Article $article) : bool
    {
        return $user->roles()->find(1) !== null || $article->user->id === $user->id;
    }
}
