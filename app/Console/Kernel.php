<?php

namespace App\Console;

use App\Models\User;
use App\Models\Lobby;

use Steam;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) : void
    {
        $schedule->call(function() {
            Lobby::all()->each(function(Lobby $lobby) {
                if($lobby->users->count() == 0) {
                    $server = $lobby->gameInfo->server;
                    $server->reserved = false;
                    $server->save();

                    $lobby->delete();
                }
            });
        })->everyFiveMinutes();

        $schedule->call(function() {
            User::all()->each(function(User $user) {
                if($user->updated_at->addDay() > Carbon::now()) return;

                /** @noinspection PhpUndefinedMethodInspection */
                $name = Steam::user($user->steam_id)->GetPlayerSummaries()[0]->personaName;
                if($name === $user->name) return;

                $user->name = $name;
                $user->save();
            });
        })->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() : void
    {
        require base_path('routes/console.php');
    }
}
