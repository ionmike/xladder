<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\Team;
use App\Models\Match;
use App\Models\Lobby;

use App\Events\Match\MatchCreated;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateMatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $lobby;

    /**
     * Create a new job instance.
     *
     * @param  \App\Models\Lobby  $lobby
     */
    public function __construct(Lobby $lobby)
    {
        $this->lobby = $lobby;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() : void
    {
        // Check twice. Sneaky users is sneaky.
        /** @noinspection PhpUndefinedMethodInspection */
        if ($this->lobby->users()->count() != $this->lobby->gameInfo->users_need) return;

        // TODO there must be a ELO algorithm.
        $shuffledCollection = $this->lobby->users->shuffle();

        $divideSize = $this->lobby->gameInfo->users_need / 2;
        $teamAUsers = $shuffledCollection->slice(0, $divideSize);
        $teamBUsers = $shuffledCollection->slice($divideSize, $divideSize);

        /** @var Team $teamA */
        $teamA = Team::create();
        /** @var Team $teamB */
        $teamB = Team::create();

        $teamA->users()->saveMany($teamAUsers);
        $teamB->users()->saveMany($teamBUsers);

        $match = new Match();
        $match->teamA()->associate($teamA);
        $match->teamB()->associate($teamB)->save();

        $this->lobby->gameInfo->lobby()->dissociate();
        $this->lobby->gameInfo->match()->associate($match)->save();

        $this->lobby->users->each(function(User $user) use ($match) {
            $user->match()->associate($match)->save();
        });

        broadcast(new MatchCreated($this->lobby->id, $match));

        $this->lobby->delete();
    }
}
