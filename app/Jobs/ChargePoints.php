<?php

namespace App\Jobs;

use App\Models\Match;

use App\Events\Match\MatchFinished;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ChargePoints implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $match, $status;

    /**
     * Create a new job instance.
     *
     * @param  \App\Models\Match  $match
     * @param  string  $status
     */
    public function __construct(Match $match, string $status)
    {
        $this->match = $match;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() : void
    {
        foreach ($this->match->users as $user) {
            $user->match()->dissociate()->save();
        }

        $server = $this->match->gameInfo->server;
        $server->reserved = false;
        $server->save();

        if($this->status === 'finished') {
            if ($this->match->team_a_score > $this->match->team_b_score) {
                /** @noinspection PhpUndefinedFieldInspection */
                $this->match->teamA->users->each->addPoints(config('matchmaking.points.win'));
                /** @noinspection PhpUndefinedFieldInspection */
                $this->match->teamB->users->each->subtractPoints(config('matchmaking.points.loose'));
            }

            if ($this->match->team_a_score < $this->match->team_b_score) {
                /** @noinspection PhpUndefinedFieldInspection */
                $this->match->teamB->users->each->addPoints(config('matchmaking.points.win'));
                /** @noinspection PhpUndefinedFieldInspection */
                $this->match->teamA->users->each->subtractPoints(config('matchmaking.points.loose'));
            }
        }

        broadcast(new MatchFinished($this->match));
    }
}
