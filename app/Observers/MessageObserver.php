<?php

namespace App\Observers;

use App\Models\Message;

use App\Events\Message\MessageCreated;
use App\Events\Message\MessageUpdated;
use App\Events\Message\MessageDeleting;

class MessageObserver
{
    /**
     * Listen to the Message created event.
     *
     * @param  \App\Models\Message  $message
     * @return void
     */
    public function created(Message $message) : void
    {
        broadcast(new MessageCreated($message))->toOthers();
    }

    /**
     * Listen to the Message updated event.
     *
     * @param  \App\Models\Message  $message
     * @return void
     */
    public function updated(Message $message) : void
    {
        broadcast(new MessageUpdated($message))->toOthers();
    }

    /**
     * Listen to the Message deleting event.
     *
     * @param  \App\Models\Message  $message
     * @return void
     */
    public function deleting(Message $message) : void
    {
        /** @noinspection PhpParamsInspection */
        broadcast(new MessageDeleting($message->id, $message->messageable))->toOthers();
    }
}