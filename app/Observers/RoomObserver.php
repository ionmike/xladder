<?php

namespace App\Observers;

use App\Models\Room;

class RoomObserver
{
    /**
     * Listen to the Room deleting event.
     *
     * @param  \App\Models\Room  $room
     * @return void
     */
    public function deleting(Room $room) : void
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $room->messages()->delete();
    }
}