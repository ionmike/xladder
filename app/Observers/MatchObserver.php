<?php

namespace App\Observers;

use App\Models\Match;

use App\Events\Match\MatchUpdated;

class MatchObserver
{
    /**
     * Listen to the Match updated event.
     *
     * @param  \App\Models\Match  $match
     * @return void
     */
    public function updated(Match $match) : void
    {
        broadcast(new MatchUpdated($match));
    }
}