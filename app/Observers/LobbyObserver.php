<?php

namespace App\Observers;

use App\Models\Lobby;

use App\Events\Lobby\LobbyDeleting;

class LobbyObserver
{
    /**
     * Listen to the Lobby deleting event.
     *
     * @param  \App\Models\Lobby  $lobby
     * @return void
     */
    public function deleting(Lobby $lobby) : void
    {
        broadcast(new LobbyDeleting($lobby->id));

        $lobby->room->delete();
    }
}