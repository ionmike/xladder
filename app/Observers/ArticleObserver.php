<?php

namespace App\Observers;

use App\Models\Article;

use App\Events\Article\ArticleCreated;
use App\Events\Article\ArticleUpdated;
use App\Events\Article\ArticleDeleting;

class ArticleObserver
{
    /**
     * Listen to the Article created event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function created(Article $article) : void
    {
        broadcast(new ArticleCreated($article))->toOthers();
    }

    /**
     * Listen to the Article updated event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function updated(Article $article) : void
    {
        broadcast(new ArticleUpdated($article))->toOthers();
    }

    /**
     * Listen to the Article deleting event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function deleting(Article $article) : void
    {
        broadcast(new ArticleDeleting($article->id));

        /** @noinspection PhpUndefinedMethodInspection */
        $article->messages()->delete();
    }
}