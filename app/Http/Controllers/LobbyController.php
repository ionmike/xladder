<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Lobby;
use App\Models\Server;
use App\Models\GameInfo;

use App\Events\Lobby\LobbyCreated;
use App\Events\Lobby\LobbyUpdated;

use Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class LobbyController extends Controller
{
    /**
     * Display lobbies.
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index() : LengthAwarePaginator
    {
        return Lobby::latest()->paginate(5);
    }

    /**
     * Store lobby with request options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Lobby|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Find any not reserved server from selected region.
        $server = Server::whereRegion($request['region'])->whereReserved(false)->whereActive(true)->get();

        if($server->isEmpty()) {
            return response(['message' => 'Cannot find free server. Try again later.', 'code' => 417], 417);
        }

        $server = $server->random();

        // Reserve selected server.
        $server->reserved = true;
        $server->save();

        /** @var Lobby $lobby */
        $lobby = Lobby::create();

        /**
         * Make game info for lobby.
         *
         * @var GameInfo $gameInfo
         */
        $gameInfo = new GameInfo([
            'users_need' => $request['users_need'],
            'mode' => $request['mode'],
            'map' => $request['map']
        ]);

        /**
         * Chat room for lobby.
         *
         * @var Room $room
         */
        $room = Room::create([
            'name' => 'Lobby #'. $lobby->id
        ]);

        $gameInfo->server()->associate($server);
        $gameInfo->lobby()->associate($lobby)->save();
        $room->lobby()->associate($lobby)->save();

        broadcast(new LobbyCreated($lobby));

        return $lobby->load('gameInfo');
    }

    /**
     * Display lobby.
     *
     * @param  \App\Models\Lobby  $lobby
     * @return \App\Models\Lobby
     */
    public function show(Lobby $lobby) : Lobby
    {
        return $lobby;
    }

    /**
     * Join lobby method. Requested user associates with lobby.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lobby  $lobby
     * @return \App\Models\Lobby|\Illuminate\Http\JsonResponse
     */
    public function joinLobby(Request $request, Lobby $lobby)
    {
        if($lobby->users->count() >= $lobby->gameInfo->users_need) {
            return response(['message' => 'Max players reached. Join other lobby.', 'code' => 417], 417);
        }

        if($request->user()->match != null) {
            return response(['message' => 'You can not join until you are in match.', 'code' => 417], 417);
        }

        $request->user()->lobby()->associate($lobby)->save();
        $lobby->load('users');

        broadcast(new LobbyUpdated($lobby));

        return $lobby;
    }

    /**
     * Leave lobby method. Requested user dissociates with lobby.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lobby  $lobby
     * @return \App\Models\Lobby
     */
    public function leaveLobby(Request $request, Lobby $lobby) : Lobby
    {
        $request->user()->lobby()->dissociate($lobby)->save();
        $lobby->load('users');

        broadcast(new LobbyUpdated($lobby));

        return $lobby;
    }
}
