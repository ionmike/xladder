<?php

namespace App\Http\Controllers;

use App\Models\Message;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MessageController extends Controller
{
    /**
     * Update the specified message.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \App\Models\Message
     */
    public function update(Request $request, Message $message) : Message
    {
        $message->update([
            'text' => $request['text']
        ]);

        return $message;
    }

    /**
     * Remove message.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message) : Response
    {
        $message->delete();

        return response(['message' => 'Message was deleted', 'code' => 200]);
    }
}
