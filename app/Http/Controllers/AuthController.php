<?php

namespace App\Http\Controllers;

use App\Models\User;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;

use Invisnik\LaravelSteamAuth\SteamAuth;

class AuthController extends Controller
{
    /**
     * @var  \Invisnik\LaravelSteamAuth\SteamAuth  $steam
     */
    private $steam;

    /**
     * AuthController constructor.
     *
     * @param  \Invisnik\LaravelSteamAuth\SteamAuth  $steam
     */
    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    /**
     * Get auth user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\User
     */
    public function auth(Request $request) : User
    {
        return $request->user()->load('lobby', 'match');
    }

    /**
     * Obtain token for user.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function obtainToken(Request $request) : Response
    {
        /** @var User $user */
        $user = $request->user();

        if ($user->roles()->find(3) == null) {
            return response(['message' => 'Only privileged users can obtain token.', 'code' => 403], 403);
        }

        // Destroy all existing tokens of user.
        /** @noinspection PhpUndefinedMethodInspection */
        $user->tokens()->delete();

        return response($user->createToken('Personal token for ' . $user->name)->accessToken);
    }

    /**
     * Login user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login() : RedirectResponse
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                /** @noinspection PhpUndefinedFieldInspection */
                $user = User::where('steam_id', $info->steamID64)->first();

                if (is_null($user)) {
                    /** @noinspection PhpUndefinedFieldInspection */
                    $user = User::create([
                        'name'     => $info->personaname,
                        'avatar'   => $info->avatarfull,
                        'steam_id' => $info->steamID64
                    ]);
                }

                Auth::login($user, true);

                return redirect('/');

            }
        }
        return $this->steam->redirect();
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) : Response
    {
        $request->user()->lobby()->dissociate()->save();
        $request->user()->match()->dissociate()->save();

        Auth::logout();

        return response(['message' => 'User logged out', 'code' => 200]);
    }
}
