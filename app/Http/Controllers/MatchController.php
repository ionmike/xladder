<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Match;
use App\Models\Report;

use App\Events\Match\MatchUpdated;

use DB;
use Steam;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Mintopia\VDFKeyValue\Encoder;
use Illuminate\Database\QueryException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class MatchController extends Controller
{
    /**
     * Get matches ordered by status.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index() : LengthAwarePaginator
    {
        return Match::orderByRaw(
            'FIELD(status, "starting", "not_started", "in_process", "paused", "not_finished", "finished")'
        )->latest()->paginate(20);
    }

    /**
     * Show match.
     *
     * @param  \App\Models\Match  $match
     * @return \App\Models\Match
     */
    public function show(Match $match) : Match
    {
        return $match;
    }

    /**
     * Update match.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Match  $match
     * @return \App\Models\Match|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Match $match)
    {
        $match->update([
            'team_a_score' => $request['team_a_score'] ?: 0,
            'team_b_score' => $request['team_b_score'] ?: 0,
            'status' => $request['status']
        ]);

        return $match;
    }

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Return requesting server match info.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function any(Request $request)
    {
        // Reason of this method is because server can crash in game process.
        // So when it crashes this method will hit again.
        $this->resolveServerCrush($request);

        /** @var Match $match */
        foreach(Match::whereStatus('starting')->get() as $match) {
            if(!$this->checkServer($request, $match)) continue;

            /** @noinspection PhpUndefinedFieldInspection */
            $teamAUsers = $match->teamA->users->map(function($user) {
                return ['steam_id' => $user->steam32];
            })->toArray();
            /** @noinspection PhpUndefinedFieldInspection */
            $teamBUsers = $match->teamB->users->map(function($user) {
                return ['steam_id' => $user->steam32];
            })->toArray();

            $object = [
                'match_id' => $match->id,
                'map' => $match->gameInfo->map,
                'config' => $match->gameInfo->mode,
                'team_a_users' => $teamAUsers,
                'team_b_users' => $teamBUsers
            ];

            if($request['format'] == 'vdf') {
                $encoder = new Encoder;
                $data = $encoder->encode('match_info', $object);

                return response($data);
            }

            return response($object);
        }
    }

    /**
     * Resolve server status problem.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function resolveServerCrush(Request $request) : void
    {
        /** @var Match $match */
        foreach(Match::whereIn('status', ['not_started', 'in_process', 'paused'])->get() as $match) {
            if(!$this->checkServer($request, $match)) continue;

            // Listener on MatchUpdate observer will free uo the server.
            if($match->gameInfo->server->reserved) {
                $match->update([
                    'status' => 'not_finished'
                ]);
            }
        }
    }

    /**
     * Check server request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Match  $match
     * @return bool
     */
    public function checkServer(Request $request, Match $match) : bool
    {
        return $match->gameInfo->server->ip == $request->ip() && $match->gameInfo->server->port == $request['port'];
    }

    /**
     * Swap user in match.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Match $match
     * @return \Illuminate\Http\JsonResponse
     */
    public function swap(Request $request, Match $match)
    {
        $teamAPlayer = User::whereSteamId(Steam::convertId($request['steam_id_team_a_player'], 'id64'))->first();
        $teamBPlayer = User::whereSteamId(Steam::convertId($request['steam_id_team_b_player'], 'id64'))->first();

        try {
            DB::beginTransaction();
            $match->teamA->users()->detach($teamAPlayer);
            $match->teamB->users()->detach($teamBPlayer);
            $match->teamA->users()->attach($teamBPlayer);
            $match->teamB->users()->attach($teamAPlayer);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Players can not be swapped',
                'code' => 417
            ], 417);
        }

        broadcast(new MatchUpdated($match));

        return response()->json([
            'message' => 'Players were swapped',
            'code' => 200
        ]);
    }

    /**
     *  Report user in match.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Match  $match
     * @return \Illuminate\Http\JsonResponse
     */
    public function report(Request $request, Match $match) : JsonResponse
    {
        $user = User::whereSteamId(Steam::convertId($request['steam_id'], 'id64'))->first();

        $report = new Report();
        $report->reason = $request['reason'];
        $report->user()->associate($user);
        $report->match()->associate($match)->save();
        $report->user->subtractPoints(config('matchmaking.points.report'));

        return response()->json([
            'message' => 'User was reported',
            'code' => 200
        ]);
    }
}
