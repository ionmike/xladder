<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Message;

use App\Events\Article\ArticleUpdated;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ArticleController extends Controller
{
    /**
     * Display articles.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index() : LengthAwarePaginator
    {
        return Article::latest()->paginate(5);
    }

    /**
     * Store an article.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Article
     */
    public function store(Request $request) : Article
    {
        $article = new Article([
            'title' => $request['title'],
            'content' => $request['content']
        ]);

        $article->user()->associate($request->user())->save();

        return $article;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \App\Models\Article
     */
    public function show(Article $article) : Article
    {
        return $article;
    }

    /**
     * Update article.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \App\Models\Article
     */
    public function update(Request $request, Article $article) : Article
    {
        $article->update([
            'title' => $request['title'],
            'content' => $request['content']
        ]);

        return $article;
    }

    /**
     * Remove article.
     *
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article) : Response
    {
        $article->delete();

        return response(['message' => 'Article was deleted', 'code' => 200]);
    }

    /**
     * Return messages in article.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function indexMessages(Article $article) : LengthAwarePaginator
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $article->messages()->orderBy('created_at', 'desc')->paginate(10);
    }

    /**
     * Store a message in article.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \App\Models\Message
     */
    public function storeMessage(Request $request, Article $article) : Message
    {
        $message = new Message([
            'text' => $request['text']
        ]);

        $message->user()->associate($request->user());
        $article->messages()->save($message);

        // Broadcast article updated when message sent.
        // Message count on articles page will be updated for specified article.
        broadcast(new ArticleUpdated($article));

        return $message;
    }
}
