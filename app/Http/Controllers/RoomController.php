<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Message;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

class RoomController extends Controller
{
    /**
     * Return room info.
     *
     * @param  \App\Models\Room  $room
     * @return \App\Models\Room
     */
    public function show(Room $room) : Room
    {
        return $room;
    }

    /**
     * Return messages from room.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Database\Eloquent\Collection|Message[]
     */
    public function indexMessages(Room $room) : Collection
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $room->messages()->latest()->limit(50)->get()->reverse();
    }

    /**
     * Store a message in room.
     *
     * @param  \App\Models\Room  $room
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Message
     */
    public function storeMessage(Room $room, Request $request) : Message
    {
        $message = new Message([
            'text' => $request['text']
        ]);

        $message->user()->associate($request->user());
        $room->messages()->save($message);

        return $message;
    }
}
