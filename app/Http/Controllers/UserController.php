<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class UserController extends Controller
{
    /**
     * Return all users.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index() : LengthAwarePaginator
    {
        return User::orderBy('rank', 'desc')->paginate(10);
    }

    /**
     * Find user by name, steam_id or rank.
     *
     * @param  Request  $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(Request $request) : LengthAwarePaginator
    {
        $keyword = $request['keyword'];

        if(strlen($keyword) < 3) return response('Enter a keyword (larger 3 symbols)');

        return User::where(function ($query) use ($keyword) {
            $query->where("name", "LIKE","%$keyword%")
                ->orWhere("steam_id", "LIKE", "%$keyword%")
                ->orWhere("rank", $keyword);
        })->paginate(10);
    }

    /**
     * Display the specified user.
     *
     * @param  \App\Models\User  $user
     * @return \App\Models\User
     */
    public function show(User $user) : User
    {
        return $user;
    }

    /**
     * Show statistic of user.
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function stat(User $user) : JsonResponse
    {
        return response()->json([
            'games_played' => $user->matches->count(),
            'games_won' => $user->matches_won->count(),
            'reports' => $user->reports()->count()
            // TODO add some fun stuff
        ]);
    }
}
