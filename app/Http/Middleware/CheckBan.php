<?php

namespace App\Http\Middleware;

use Closure;

class CheckBan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->banned_at) {
            return response()->json([
                'message' => 'Banned user can not access API',
                'code' => 403
            ], 403);
        }

        return $next($request);
    }
}
