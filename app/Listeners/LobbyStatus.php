<?php

namespace App\Listeners;

use App\Jobs\CreateMatch as CreateMatchJob;

use App\Events\Lobby\LobbyUpdated;

class LobbyStatus
{
    /**
     * Handle the event.
     *
     * @param  \App\Events\Lobby\LobbyUpdated  $event
     * @return void
     */
    public function handle(LobbyUpdated $event) : void
    {
        if($event->lobby->users->count() >= $event->lobby->gameInfo->users_need) {
            dispatch((new CreateMatchJob($event->lobby))->delay(5));
        }
    }
}
