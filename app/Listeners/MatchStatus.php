<?php

namespace App\Listeners;

use App\Jobs\ChargePoints as ChargePoinsJob;

use App\Events\Match\MatchUpdated;

class MatchStatus
{
    /**
     * Handle the event.
     *
     * @param  \App\Events\Match\MatchUpdated  $event
     * @return void
     */
    public function handle(MatchUpdated $event) : void
    {
        if($event->match->status === 'finished') {
            dispatch((new ChargePoinsJob($event->match, 'finished'))->delay(5));
        }

        if($event->match->status === 'not_finished') {
            dispatch((new ChargePoinsJob($event->match, 'not_finished'))->delay(5));
        }
    }
}
