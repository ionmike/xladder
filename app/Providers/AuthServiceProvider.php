<?php

namespace App\Providers;

use App\Models\Match;
use App\Models\Message;
use App\Models\Article;

use App\Policies\MatchPolicy;
use App\Policies\MessagePolicy;
use App\Policies\ArticlePolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Match::class => MatchPolicy::class,
        Message::class => MessagePolicy::class,
        Article::class => ArticlePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
