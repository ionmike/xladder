<?php

namespace App\Providers;

use App\Models\Room;
use App\Models\Lobby;
use App\Models\Match;
use App\Models\Message;
use App\Models\Article;

use App\Observers\RoomObserver;
use App\Observers\LobbyObserver;
use App\Observers\MatchObserver;
use App\Observers\MessageObserver;
use App\Observers\ArticleObserver;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'room' => Room::class,
            'article' => Article::class,
        ]);

        Room::observe(RoomObserver::class);
        Lobby::observe(LobbyObserver::class);
        Match::observe(MatchObserver::class);
        Message::observe(MessageObserver::class);
        Article::observe(ArticleObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
