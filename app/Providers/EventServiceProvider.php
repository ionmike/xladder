<?php

namespace App\Providers;

use App\Listeners\LobbyStatus;
use App\Listeners\MatchStatus;

use App\Events\Lobby\LobbyUpdated;
use App\Events\Match\MatchUpdated;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        LobbyUpdated::class => [
            LobbyStatus::class
        ],

        MatchUpdated::class => [
            MatchStatus::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
