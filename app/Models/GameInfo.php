<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\GameInfo
 *
 * @property int $id
 * @property int $lobby_id
 * @property int $match_id
 * @property int $server_id
 * @property string $map
 * @property string $mode
 * @property int $users_need
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Lobby $lobby
 * @property-read \App\Models\Match $match
 * @property-read \App\Models\Server $server
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereLobbyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereMap($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereMatchId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereMode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereServerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GameInfo whereUsersNeed($value)
 * @mixin \Eloquent
 */
class GameInfo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mode', 'map', 'users_need'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $with = [
        'server'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'lobby_id', 'match_id', 'server_id', 'created_at', 'updated_at'
    ];

    /**
     * Lobby has server.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function server() : BelongsTo
    {
        return $this->belongsTo(Server::class);
    }

    /**
     * GameInfo belongs to lobby.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lobby() : BelongsTo
    {
        return $this->belongsTo(Lobby::class);
    }

    /**
     * GameInfo belongs to match.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match() : BelongsTo
    {
        return $this->belongsTo(Match::class);
    }
}
