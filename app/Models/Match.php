<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Match
 *
 * @property int $id
 * @property int $team_a_id
 * @property int $team_b_id
 * @property int $team_a_score
 * @property int $team_b_score
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\GameInfo $gameInfo
 * @property-read \App\Models\Team $teamA
 * @property-read \App\Models\Team $teamB
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Report[] $reports
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereTeamAId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereTeamAScore($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereTeamBId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereTeamBScore($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Match whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Match extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_a_score', 'team_b_score', 'status'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $with = [
        'teamA', 'teamB', 'gameInfo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'team_a_id', 'team_b_id'
    ];

    /**
     * Match has many users currently playing match.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users() : HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * Match has 2 teams. This is A team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teamA() : BelongsTo
    {
        return $this->belongsTo(Team::class, 'team_a_id');
    }

    /**
     * And this is B team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teamB() : BelongsTo
    {
        return $this->belongsTo(Team::class, 'team_b_id');
    }

    /**
     * Match has some info.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gameInfo() : HasOne
    {
        return $this->hasOne(GameInfo::class);
    }

    /**
     * Match has many reports.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports(): HasMany
    {
        return $this->hasMany(Report::class);
    }

    /**
     * Get winning team in match.
     *
     * @return \App\Models\Team
     */
    public function winningTeam() : Team
    {
        return $this->team_a_score > $this->team_b_score ? $this->teamA : $this->teamB;
    }
}
