<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Server
 *
 * @property int $id
 * @property string $region
 * @property string $name
 * @property string $ip
 * @property int $port
 * @property bool $reserved
 * @property bool $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GameInfo[] $gameInfos
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server wherePort($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereReserved($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server whereActive($value)
 * @mixin \Eloquent
 */
class Server extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'ip', 'port', 'region'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'reserved' => 'boolean'
    ];

    /**
     * Server has many game infos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gameInfos() : HasMany
    {
        return $this->hasMany(GameInfo::class);
    }
}
