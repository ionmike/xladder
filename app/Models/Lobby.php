<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Lobby
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\GameInfo $gameInfo
 * @property-read \App\Models\Room $room
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lobby whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lobby whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lobby whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Lobby extends Model
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $with = [
        'users', 'gameInfo', 'room'
    ];

    /**
     * Lobby has users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users() : HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * Lobby has some info about game.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gameInfo() : HasOne
    {
        return $this->hasOne(GameInfo::class);
    }

    /**
     * Lobby has chat room.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function room() : HasOne
    {
        return $this->hasOne(Room::class);
    }
}
