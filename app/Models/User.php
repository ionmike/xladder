<?php

namespace App\Models;

use App\Events\User\RankUpdated;

use Steam;
use Illuminate\Support\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Passport\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int $lobby_id
 * @property int $match_id
 * @property string $name
 * @property string $avatar
 * @property string $steam_id
 * @property int $rank
 * @property string $remember_token
 * @property \Carbon\Carbon $banned_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article[] $articles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Support\Collection|Match[] $matches
 * @property-read \Illuminate\Support\Collection|Match[] $matches_won
 * @property-read \App\Models\Lobby $lobby
 * @property-read \App\Models\Match $match
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $messages
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Report[] $reports
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Team[] $teams
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereBannedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereLobbyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereMatchId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRank($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereSteamId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'steam_id', 'avatar', 'rank'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $with = [
        'roles'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'banned_at' => 'date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'remember_token', 'lobby_id', 'match_id', 'pivot'
    ];

    /**
     * User has roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles() : BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * User has articles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles() : HasMany
    {
        return $this->hasMany(Article::class);
    }

    /**
     * User has lobby he currently in.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lobby() : BelongsTo
    {
        return $this->belongsTo(Lobby::class);
    }

    /**
     * User has match he currently in.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match() : BelongsTo
    {
        return $this->belongsTo(Match::class);
    }

    /**
     * User has teams he played with.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams() : BelongsToMany
    {
        return $this->belongsToMany(Team::class);
    }

    /**
     * User has messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages() : HasMany
    {
        return $this->hasMany(Message::class);
    }

    /**
     * User has many reports.
     *
     * @return HasMany
     */
    public function reports() : HasMany
    {
        return $this->hasMany(Report::class);
    }

    /**
     * Get matches attribute from the model.
     *
     * @return \Illuminate\Support\Collection|Match[]
     */
    public function getMatchesAttribute() : Collection
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->teams->map->matches->collapse();
    }

    /**
     * Get matches won attribute from the model.
     *
     * @return \Illuminate\Support\Collection|Match[]
     */
    public function getMatchesWonAttribute() : Collection
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->teams->map->matches_won->collapse();
    }

    /**
     * Add point for user.
     *
     * @param  int  $points
     */
    public function addPoints(int $points) : void
    {
        $this->rank += $points;
        $this->save();

        broadcast(new RankUpdated($this));
    }

    /**
     * Subtract points from user.
     *
     * @param  int  $points
     */
    public function subtractPoints(int $points) : void
    {
        $this->rank -= $points;
        $this->save();
    }

    /**
     * Convert steam_id to Steam32.
     *
     * @return string
     */
    public function getSteam32Attribute() : string
    {
        return Steam::convertId($this->steam_id, 'id32');
    }
}
