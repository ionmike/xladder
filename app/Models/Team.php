<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Team
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Match[] $matches
 * @property-read \Illuminate\Database\Eloquent\Collection|Match[] $matches_won
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Match[] $matchesAsTeamA
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Match[] $matchesAsTeamB
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $substitutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Team whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Team whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Team extends Model
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $with = [
        'users', 'substitutions'
    ];

    /**
     * Team has many players.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() : BelongsToMany
    {
        return $this->belongsToMany(User::class)->wherePivot('substitution', false);
    }

    /**
     * Team has many subs.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function substitutions() : BelongsToMany
    {
        return $this->belongsToMany(User::class)->wherePivot('substitution', true);
    }

    /**
     * Team played matches as A team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matchesAsTeamA() : HasMany
    {
        return $this->hasMany(Match::class, 'team_a_id');
    }

    /**
     * Team played matches as B team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matchesAsTeamB() : HasMany
    {
        return $this->hasMany(Match::class, 'team_b_id');
    }

    /**
     * Get matches attribute from the model.
     *
     * @return \Illuminate\Database\Eloquent\Collection|Match[]
     */
    public function getMatchesAttribute() : Collection
    {
        return $this->matchesAsTeamA->merge($this->matchesAsTeamB);
    }

    /**
     * Get matches won attribute from the model.
     *
     * @return \Illuminate\Database\Eloquent\Collection|Match[]
     */
    public function getMatchesWonAttribute() : Collection
    {
        $matchesWon = new Collection();

        $this->matches->each(function(Match $match) use ($matchesWon) {
            if($match->winningTeam()->id == $this->id) {
                $matchesWon->add($match);
            }
        });

        return $matchesWon;
    }
}
