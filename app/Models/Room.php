<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * App\Models\Room
 *
 * @property int $id
 * @property int $lobby_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Lobby $lobby
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $messages
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Room whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Room whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Room whereLobbyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Room whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Room whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Room extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'lobby_id'
    ];

    /**
     * Room has many messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function messages() : MorphMany
    {
        return $this->morphMany(Message::class, 'messageable');
    }

    /**
     * Room has lobby.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lobby() : BelongsTo
    {
        return $this->belongsTo(Lobby::class);
    }
}
