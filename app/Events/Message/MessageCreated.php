<?php

namespace App\Events\Message;

use App\Models\Room;
use App\Models\Article;
use App\Models\Message;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @param  \App\Models\Message  $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PresenceChannel
     */
    public function broadcastOn() : PresenceChannel
    {
        if(get_class($this->message->messageable) === Room::class) {
            return new PresenceChannel('chat-room.' . $this->message->messageable_id);
        }

        if(get_class($this->message->messageable) === Article::class) {
            return new PresenceChannel('article-room.' . $this->message->messageable_id);
        }
    }
}
