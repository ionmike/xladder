<?php

namespace App\Events\Message;

use App\Models\Room;
use App\Models\Article;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageDeleting implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $messageId, $messageable;

    /**
     * Create a new event instance.
     *
     * @param  int  $messageId
     * @param  \App\Models\Room|\App\Models\Article  $messageable
     */
    public function __construct($messageId, $messageable)
    {
        $this->messageId = $messageId;
        $this->messageable = $messageable;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PresenceChannel
     */
    public function broadcastOn() : PresenceChannel
    {
        if(get_class($this->messageable) === Room::class) {
            return new PresenceChannel('chat-room.' . $this->messageable->id);
        }

        if(get_class($this->messageable) === Article::class) {
            return new PresenceChannel('article-room.' . $this->messageable->id);
        }
    }
}
