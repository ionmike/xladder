<?php

namespace App\Events\Lobby;

use App\Models\Lobby;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LobbyUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $lobby;

    /**
     * Create a new event instance.
     *
     * @param  \App\Models\Lobby  $lobby
     */
    public function __construct(Lobby $lobby)
    {
        $this->lobby = $lobby;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array
     */
    public function broadcastOn() : array
    {
        return [
            new Channel('lobbies'),
            new PresenceChannel('lobby-room.' . $this->lobby->id)
        ];
    }
}
