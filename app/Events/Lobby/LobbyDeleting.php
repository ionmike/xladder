<?php

namespace App\Events\Lobby;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LobbyDeleting implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $lobbyId;

    /**
     * Create a new event instance.
     *
     * @param  int  $lobbyId
     */
    public function __construct($lobbyId)
    {
        $this->lobbyId = $lobbyId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array
     */
    public function broadcastOn() : array
    {
        return [
            new Channel('lobbies'),
            new PresenceChannel('lobby-room.' . $this->lobbyId)
        ];
    }
}
