<?php

namespace App\Events\Match;

use App\Models\Match;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MatchCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $lobbyId, $match;

    /**
     * Create a new event instance.
     *
     * @param  int  $lobbyId
     * @param  \App\Models\Match  $match
     */
    public function __construct($lobbyId, Match $match)
    {
        $this->lobbyId = $lobbyId;
        $this->match = $match;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PresenceChannel
     */
    public function broadcastOn() : PresenceChannel
    {
        return new PresenceChannel('lobby-match-room.' . $this->lobbyId);
    }
}
