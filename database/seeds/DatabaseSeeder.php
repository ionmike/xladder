<?php

use App\Models\{
    Lobby, Team, Match, Server, GameInfo, User, Room, Article, Message, Role
};

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(Lobby::class, 4)->create();
        factory(Team::class, 8)->create();
        factory(Match::class, 4)->create();
        factory(Server::class, 8)->create();
        factory(GameInfo::class, 8)->create();
        factory(User::class, 16)->create();
        factory(Room::class, 5)->create();
        factory(Article::class, 10)->create();
        factory(Message::class, 10)->create();
        factory(Role::class, 3)->create();

        Model::reguard();
    }
}
