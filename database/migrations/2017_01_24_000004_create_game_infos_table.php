<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lobby_id')->unique()->nullable();
            $table->unsignedInteger('match_id')->unique()->nullable();
            $table->unsignedInteger('server_id')->nullable();
            $table->string('map');
            $table->string('mode');
            $table->unsignedInteger('users_need')->default(config('matchmaking.players.start_default'));
            $table->timestamps();
        });

        Schema::table('game_infos', function(Blueprint $table) {
            $table->foreign('lobby_id')->references('id')->on('lobbies')->onDelete('set null');
            $table->foreign('server_id')->references('id')->on('servers')->onDelete('set null');
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_infos');
    }
}
