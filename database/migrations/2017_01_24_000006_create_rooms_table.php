<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lobby_id')->index()->nullable();
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('rooms', function(Blueprint $table) {
            $table->foreign('lobby_id')->references('id')->on('lobbies')->onDelete('cascade');
        });

        DB::table('rooms')->insert([
            'name' => 'General'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
