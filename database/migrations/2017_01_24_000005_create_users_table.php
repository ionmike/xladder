<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('banned_at')->nullable();
            $table->unsignedInteger('lobby_id')->index()->nullable();
            $table->unsignedInteger('match_id')->index()->nullable();
            $table->string('name')->index();
            $table->string('avatar');
            $table->string('steam_id')->unique();
            $table->integer('rank')->default(1000);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('lobby_id')->references('id')->on('lobbies')->onDelete('set null');
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
