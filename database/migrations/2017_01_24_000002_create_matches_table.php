<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('team_a_id')->index();
            $table->unsignedInteger('team_b_id')->index();
            $table->unsignedInteger('team_a_score')->default(0);
            $table->unsignedInteger('team_b_score')->default(0);
            $table->enum('status', ['starting', 'not_started', 'in_process', 'paused', 'not_finished', 'finished'])
                ->default('starting')->index();
            $table->timestamps();
        });

        Schema::table('matches', function (Blueprint $table) {
            $table->foreign('team_a_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('team_b_id')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
