<?php

use App\Models\{
    Lobby, Match, Team, Server, GameInfo, User, Room, Article, Message, Role
};

use Faker\Generator;

$factory->define(Lobby::class, function () {
    return [];
});

$factory->define(Team::class, function () {
    return [];
});

$factory->define(Match::class, function (Generator $faker) {
    return [
        'team_a_id' => function () {
            return factory(Team::class)->create()->id;
        },
        'team_b_id' => function () {
            return factory(Team::class)->create()->id;
        },
        'team_a_score' => $faker->numberBetween(0, 6000),
        'team_b_score' => $faker->numberBetween(0, 6000),
        'status' => $faker->randomElement(['starting', 'not_started', 'in_process', 'paused', 'not_finished', 'finished'])
    ];
});

$factory->define(Server::class, function (Generator $faker) {
    return [
        'name' => $faker->name,
        'region' => $faker->randomElement(['ru', 'eu']),
        'ip' => $faker->ipv4,
        'port' => $faker->numberBetween(27015, 27030)
    ];
});

$factory->define(GameInfo::class, function (Faker\Generator $faker) {
    return [
        'mode' => $faker->lastName,
        'map' => $faker->firstName,
        'users_need' => $faker->numberBetween(1, 8)
    ];
});

$factory->define(User::class, function (Generator $faker) {
    return [
        'name' => $faker->name,
        'avatar' => 'http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_full.jpg',
        'steam_id' => $faker->randomNumber(8),
        'rank' => $faker->numberBetween(1000, 3000),
        'remember_token' => str_random(40),
    ];
});

$factory->define(Room::class, function (Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

$factory->define(Article::class, function (Generator $faker) {
    return [
        'title' => $faker->title,
        'content' => $faker->text(),
        'user_id' => function () {
            return factory(User::class)->create()->id;
        }
    ];
});

$factory->define(Message::class, function (Generator $faker) {
    return [
        'messageable_id' => $faker->numberBetween(1, 10),
        'messageable_type' => $faker->randomElement(['article', 'room']),
        'text' => $faker->text(100),
        'user_id' => $faker->numberBetween(1, 10)
    ];
});

$factory->define(Role::class, function (Generator $faker) {
    return [
        'name' => $faker->name
    ];
});
