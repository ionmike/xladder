module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "es6": true
    },
    plugins: ["vue"],
    "globals": {
        "autosize": true,
        "iziToast": true,
        "axios": true,
        "TWEEN": true,
        "Echo": true,
        "Vue": true,
        "_": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "vue/jsx-uses-vars": 2,
        "indent": [
            "error",
            4
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "never"
        ]
    }
};