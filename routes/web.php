<?php

Route::get('/', function () {
    if(Auth::check()) {
        return Auth::user()->banned_at ? view('banned') : view('app');
    } else {
        return view('welcome');
    }
});

Route::get ('/auth/login',  'AuthController@login');
Route::post('/auth/logout', 'AuthController@logout');
