<?php

Route::get('/users',                       'UserController@index'           );
Route::get('/users/search',                'UserController@search'          );
Route::get('/users/{user}',                'UserController@show'            );
Route::get('/users/{user}/stat',           'UserController@stat'            );

Route::get('/rooms/{room}',                'RoomController@show'            );
Route::get('/rooms/{room}/messages',       'RoomController@indexMessages'   );

Route::get('/articles',                    'ArticleController@index'        );
Route::get('/articles/{article}',          'ArticleController@show'         );
Route::get('/articles/{article}/messages', 'ArticleController@indexMessages');

Route::get('/matches',                     'MatchController@index'          );
Route::get('/matches/any',                 'MatchController@any'            );
Route::get('/matches/{match}',             'MatchController@show'           );

Route::get('/lobbies',                     'LobbyController@index'          );
Route::get('/lobbies/{lobby}',             'LobbyController@show'           );

Route::middleware(['auth:api', 'not.banned.user'])->group(function () {

    Route::get   ('/auth/user',                   'AuthController@auth'           );
    Route::get   ('/auth/token',                  'AuthController@obtainToken'    );

    Route::post  ('/articles',                    'ArticleController@store'       )->middleware('can:store,' . \App\Models\Article::class);
    Route::patch ('/articles/{article}',          'ArticleController@update'      )->middleware('can:update,article');
    Route::delete('/articles/{article}',          'ArticleController@destroy'     )->middleware('can:destroy,article');

    Route::patch ('/messages/{message}',          'MessageController@update'      )->middleware('can:update,message');
    Route::delete('/messages/{message}',          'MessageController@destroy'     )->middleware('can:destroy,message');

    Route::post  ('/articles/{article}/messages', 'ArticleController@storeMessage');
    Route::post  ('/rooms/{room}/messages',       'RoomController@storeMessage'   );

    Route::patch ('/matches/{match}',             'MatchController@update'        )->middleware('can:update,match');
    Route::post  ('/matches/{match}/swap',        'MatchController@swap'          );
    Route::post  ('/matches/{match}/report',      'MatchController@report'        );

    Route::post  ('/lobbies',                     'LobbyController@store'         );

    Route::post  ('/lobbies/{lobby}/join',        'LobbyController@joinLobby'     );
    Route::post  ('/lobbies/{lobby}/leave',       'LobbyController@leaveLobby'    );

});

if (App::environment('local')) {
    Route::get('/token/{user}', function ($user) {
        return App\Models\User::findOrFail($user)->createToken('API')->accessToken;
    });
}
