<?php

Broadcast::channel('chat-room.{room}', function ($user) {
    return [
        'id' => $user->id,
        'name' => $user->name,
        'rank' => $user->rank,
        'roles' => $user->roles
    ];
});

Broadcast::channel('article-room.{post}', function ($user) {
    return [
        'name' => $user->name
    ];
});

Broadcast::channel('lobby-room.{lobby}', function ($user) {
    return [
        'id' => $user->id,
        'name' => $user->name
    ];
});
Broadcast::channel('lobby-match-room.{lobby}', function ($user) {
    return [
        'id' => $user->id,
        'name' => $user->name
    ];
});

Broadcast::channel('match-room.{match}', function ($user) {
    return [
        'id' => $user->id,
        'name' => $user->name
    ];
});

Broadcast::channel('user-notification-room.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});