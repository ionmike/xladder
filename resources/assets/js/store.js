
import Vuex from 'vuex'

const state = {
    user: {},

    gameInfo: {
        modes: [
            {
                value: 'acemode',
                text: '4x4 Acemode',
                usersNeed: 8
            },
            {
                value: 'promod',
                text: '4x4 Promod',
                usersNeed: 8
            },
            {
                value: '1v1practiceogl',
                text: '1x1 Practiceogl',
                usersNeed: 4
            },
            {
                value: 'pm2v2',
                text: '2x2 Promode',
                usersNeed: 4
            },
            {
                value: 'am3v3',
                text: '3x3 Acemode',
                usersNeed: 6
            },
            {
                value: undefined,
                text: 'Unknown',
                usersNeed: 0
            }
        ],
        regions: [
            {
                value: 'eu',
                text: 'EU'
            },
            {
                value: 'ru',
                text: 'RU'
            },
            {
                value: undefined,
                text: 'Unknown'
            }
        ],
        maps: [
            {
                value: 'c2m1_highway',
                text: 'Dark Carnival',
                image: '/images/maps/dark_carnival.png'
            },
            {
                value: 'c5m1_waterfront',
                text: 'The Parish',
                image: '/images/maps/parish.png'
            },
            {
                value: 'c3m1_plankcountry',
                text: 'Hard Rain',
                image: '/images/maps/hard_rain.png'
            },
            {
                value: 'c1m1_hotel',
                text: 'Dead Center',
                image: '/images/maps/dead_center.png'
            },
            {
                value: 'c4m1_milltown_a',
                text: 'Swamp Fever',
                image: '/images/maps/swamp_fever.png'
            },
            {
                value: undefined,
                text: 'Unknown',
                image: '/images/maps/unknown.png'
            }
        ]
    }
}

const mutations = {
    setUser     (state, user)  { state.user = user        },
    setUserRank (state, rank)  { state.user.rank = rank   },
    setUserLobby(state, lobby) { state.user.lobby = lobby },
    setUserMatch(state, match) { state.user.match = match }
}

const actions = {
    setUser     ({ commit }, user)  { commit('setUser',  user)      },
    setUserRank ({ commit }, rank)  { commit('setUserRank', rank)   },
    setUserLobby({ commit }, lobby) { commit('setUserLobby', lobby) },
    setUserMatch({ commit }, match) { commit('setUserMatch', match) }
}

const getters = {
    getMap:    state => value => _.find(state.gameInfo.maps,    map    => map.value    === value),
    getMode:   state => value => _.find(state.gameInfo.modes,   mode   => mode.value   === value),
    getRegion: state => value => _.find(state.gameInfo.regions, region => region.value === value)
}

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})
