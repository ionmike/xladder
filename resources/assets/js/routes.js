
import VueRouter from 'vue-router'

// Common views
import Home from './views/Home'
import Ranks from './views/Ranks'
import Lobbies from './views/Lobbies'
import Matches from './views/Matches'

// Pages
import Article from './pages/Article'
import User from './pages/User'
import Lobby from './pages/Lobby'
import Match from './pages/Match'

const routes = [
    {
        path: '/',
        redirect: { name: 'home' }
    },
    {
        name: 'home',
        path: '/home',
        component: Home
    },
    {
        name: 'lobbies',
        path: '/lobbies',
        component: Lobbies
    },
    {
        name: 'matches',
        path: '/matches',
        component: Matches
    },
    {
        name: 'ranks',
        path: '/ranks',
        component: Ranks
    },
    {
        name: 'article',
        path: '/article/:id',
        component: Article,
        props: true
    },
    {
        name: 'user',
        path: '/user/:id',
        component: User,
        props: true
    },
    {
        name: 'lobby',
        path: '/lobby/:id',
        component: Lobby,
        props: true
    },
    {
        name: 'match',
        path: '/match/:id',
        component: Match,
        props: true
    }
]

export default new VueRouter({
    routes,
    linkActiveClass: 'is-active'
})
