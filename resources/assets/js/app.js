
import './bootstrap'

import moment from 'moment'
import VueMarkdown from 'vue-markdown'
import VueProgressBar from 'vue-progressbar'

import store from './store'
import router from './routes'

Vue.prototype.$moment = moment
Vue.prototype.$eventBus = new Vue()

Vue.use(VueProgressBar, {
    color: 'rgb(0, 209, 178)',
    failedColor: 'red',
    height: '1px'
})

Vue.component('vue-markdown', VueMarkdown)

import Navbar from './components/Navbar'
import RankBoard from './components/RankBoard'

Vue.filter('pluralize', (value, string) => {
    return value !== 1 ? `${string}s` : `${string}`
})

new Vue({
    el: '#app',

    components: {
        'navbar' : Navbar,
        'rank-board' : RankBoard
    },

    computed: {
        user() {
            return this.$store.state.user
        },

        // NOTE this is horrible code but it works. Maybe refactor?
        userCanJoinOrLeaveLobby() {
            return this.user.match === null
                && this.user.lobby === null
                || (this.user.lobby !== null && this.user.lobby.users.length !== this.user.lobby.game_info.users_need)
        }
    },

    created() {
        /*
         * Get authenticated user and show page components.
         * If user have lobbies set save it in store and sub to Echo channels.
         * Also subscribe to user notification channel.
         */
        axios.get('/api/auth/user')
            .then(response => {
                this.$store.dispatch('setUser', response.data)

                if(response.data.lobby) this.subscribeToLobbyChannel(response.data.lobby.id)
                if(response.data.match) this.subscribeToMatchChannel(response.data.match.id)

                Echo.join(`user-notification-room.${response.data.id}`)
                    .listen('User.RankUpdated', (event) => {
                        this.$store.dispatch('setUserRank', event.user.rank)
                        this.$eventBus.$emit('rank-board-show', event.user.rank)
                    })
            })

        // When user join lobby event should be fired, on that moment we subscribe to lobby channel.
        this.$eventBus.$on('user-joined-lobby', (lobby) => {
            let possibleOldLobby = this.$store.state.user.lobby
            if(possibleOldLobby) this.unsubscribeFromLobbyChannel(possibleOldLobby.id)

            this.$store.dispatch('setUserLobby', lobby)

            this.subscribeToLobbyChannel(lobby.id)
        })

        // When user left lobby obviously unsubscribe from lobby channel.
        this.$eventBus.$on('user-left-lobby', (lobby) => {
            this.$store.dispatch('setUserLobby', null)

            this.unsubscribeFromLobbyChannel(lobby.id)
        })
    },

    methods: {
        /*
         * Subscribe to lobby channel.
         * Also subscribe to lobby-match channel.
         * On lobby-match channel MatchCreated event happens when match creates for full lobby.
         */
        subscribeToLobbyChannel(lobbyId) {
            Echo.join(`lobby-room.${lobbyId}`)
                .listen('Lobby.LobbyUpdated', (updatedLobby) => {
                    this.$store.dispatch('setUserLobby', updatedLobby.lobby)
                })

            Echo.join(`lobby-match-room.${lobbyId}`)
                .listen('Match.MatchCreated', (newMatch) => {
                    this.$store.dispatch('setUserLobby', null)
                    this.$store.dispatch('setUserMatch', newMatch.match)

                    let audio = new Audio('sounds/match_ready.mp3')
                    audio.play()

                    this.subscribeToMatchChannel(newMatch.match.id)
                    this.$router.push({name: 'match', params: { id: newMatch.match.id }})
                })
        },

        unsubscribeFromLobbyChannel(lobbyId) {
            Echo.leave(`lobby-room.${lobbyId}`)
            Echo.leave(`lobby-match-room.${lobbyId}`)
        },

        subscribeToMatchChannel(matchId) {
            Echo.join(`match-room.${matchId}`)
                .listen('Match.MatchFinished', (finishedMatch) => {
                    this.$store.dispatch('setUserMatch', null)
                    Echo.leave(`match-room.${finishedMatch.match.id}`)
                })
        }
    },

    store,
    router
})
