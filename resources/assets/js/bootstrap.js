
import Vue from 'vue'
import _ from 'lodash'
import axios from 'axios'
import TWEEN from 'tween.js'
import Echo from 'laravel-echo'
import iziToast from 'izitoast'
import autosize from 'autosize'

window._ = _
window.Vue = Vue
window.axios = axios
window.TWEEN = TWEEN
window.iziToast = iziToast
window.autosize = autosize

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
})

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    'X-Requested-With': 'XMLHttpRequest'
}
