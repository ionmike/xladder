<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link href="/css/app.css" rel="stylesheet">
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
</head>
<body>
<div id="app">
    <transition enter-active-class="router-view-transition fadeIn">
        <navbar v-if="user && user.id"></navbar>
    </transition>
    <transition enter-active-class="router-view-transition fadeIn"
                leave-active-class="router-view-transition fadeOut"
                mode="out-in">
        <router-view v-if="user && user.id"></router-view>
    </transition>
    <rank-board></rank-board>
    <vue-progress-bar></vue-progress-bar>
</div>
<script src="/js/app.js"></script>
</body>
</html>
