<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>XLadder</title>
    <style>
        html, body {
            background-color: #0d0f12;
            color: #e0e0e0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: normal;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 64px;
            margin: 10px 0;
        }

        .logo {
            background: url('images/logo-glitch.png') center no-repeat;
            height: 410px;
            width: 433px;
            margin: 20px;
            min-height: 100px;
            background-size: contain;
        }

        .description {
            font-size: 18px;
            color: #c5c5c5;
            margin: 10px 0;
        }

        .login-button {
            margin: 50px;
            height: 35px;
        }
    </style>
</head>
<body>
<div class="flex-center full-height">
    <div class="logo"></div>
    <div class="content">
        <div class="title">
            {{ config('app.name') }}
        </div>
        <div class="description">
            {{ config('app.name') }} is a free matchmaking system for L4D<br>
            <script>
                var sentences= [
                    'Respect all, fear none',
                    'Made by players for players',
                    'Talent wins games',
                    'Go hard or go home!',
                    'Life is a game',
                    'You just got served',
                    'There is no I in Team',
                    'Eat, sleep, play'
                ];
                var index = Math.floor(Math.random() * (sentences.length));
                document.write(sentences[index])
            </script>

        </div>
        <div  class="login-button">
            <a href="{{ url('/auth/login') }}">
                <img src="https://steamcommunity-a.akamaihd.net/public/images/signinthroughsteam/sits_01.png" width="180" height="35" border="0">
            </a>
        </div>
    </div>
</div>
</body>
</html>
