<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>XLadder</title>
    <style>
        html, body {
            background-color: #0d0f12;
            color: #e0e0e0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: normal;
            height: 100vh;
            margin: 0;
        }

        a {
            color: #8cddf1;
            text-decoration: none;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 24px;
            margin: 10px 0;
        }

        .logo {
            background: url('images/logo-glitch.png') center no-repeat;
            height: 410px;
            width: 433px;
            margin: 20px;
            min-height: 100px;
            background-size: contain;
        }

        .description {
            font-size: 18px;
            color: #c5c5c5;
            margin: 10px 0;
        }
    </style>
</head>
<body>
<div class="flex-center full-height">
    <div class="logo"></div>
    <div class="content">
        <div class="title">
            Seems like you are banned from the system.
        </div>
        <div class="description">
            Come next time, or <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">enjoy beautiful peace of the world</a>.
        </div>
    </div>
</div>
</body>
</html>
